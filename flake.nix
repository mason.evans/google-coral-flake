{
  description = "gasket and apex drivers for coral tpu";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  };

  outputs = { self, nixpkgs }: {

    nixosModules = rec {
      coral = { config, lib, ...}: with lib; {
        options = {
          mason.coral.enable = mkEnableOption "enable Coral TPU";
        };

        config = 
          with import nixpkgs { system = "x86_64-linux"; };
          mkIf config.mason.coral.enable {
            services.udev.packages = [ "${self.packages.x86_64-linux.coral}" ];

            boot.extraModulePackages = [ "${self.packages.x86_64-linux.coral}" ];
            boot.kernelModules = [ "gasket" "apex" ];

            boot.kernelPackages = linuxKernel.packages.linux_6_1; # force kernel version to match below
            # to update the kernel, update this module and change the package name above and below to match
          };
      };
      default = coral;
    };

    packages.x86_64-linux.coral =
      with import nixpkgs { system = "x86_64-linux"; };

      stdenv.mkDerivation rec {
        version = "1.0";
        kernel = linuxKernel.kernels.linux_6_1;
        kernelVersion = kernel.modDirVersion;

        name = "coral-${version}-module-${kernelVersion}";

        enableParallelBuilding = true;

        src = fetchurl {
          url = "https://packages.cloud.google.com/apt/pool/gasket-dkms_1.0-18_all_00606bc20aed9a7d2a9da7a6d51a87dbc7f275be392fb3e1131ef6f627a49168.deb";
          sha256 = "sha256-AGBrwgrtmn0qnaem1RqH28fydb45L7PhEx729iekkWg=";
        };

        # Required for compilation
        nativeBuildInputs = [
          kernel.moduleBuildDependencies
          dpkg
        ];

        phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];

        unpackPhase = ''
          mkdir -p $out
          dpkg -x $src $out
          cd $out/usr/src/gasket-${version}
        '';

        patchPhase = ''
          substituteInPlace Makefile --replace "/lib/modules/\$(KVERSION)/build" ${kernel.dev}/lib/modules/${kernelVersion}/build
        '';

                    hardeningDisable = [ "pic" "format" ];
        makeFlags = [
          "KERNELRELEASE=${kernelVersion}"                                 # 3
          "KERNEL_DIR=${kernel.dev}/lib/modules/${kernelVersion}/build"    # 4
          "INSTALL_MOD_PATH=$(out)"                                               # 5
        ];

        installPhase = ''
          mkdir -p "$out/lib/modules/${kernelVersion}"

          mv apex.ko $out/lib/modules/${kernelVersion}
          mv gasket.ko $out/lib/modules/${kernelVersion}

          rm -rf $out/usr
        '';

        meta = with lib; {
          description = "Driver for coral edge TPU";
          longDescription = ''
            Driver for coral edge tpu
          '';
          homepage = "https://gitlab.com/mason.evans/google-coral-flake";
          license = licenses.gpl3Plus;
          platforms = platforms.linux;
        };
      };
  };
}
